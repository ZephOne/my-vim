set mouse=""
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
syntax on
if has("gui_running")
    set formatoptions+=a
    set formatprg=par\ -w80
    set guifont=Hack\ Regular\ 12
    set number
    colorscheme desert
endif
